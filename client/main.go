package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"sync"
	"sync/atomic"
	"time"
)

func main() {
	serverBaseURL := os.Getenv("SERVER_BASE_URL")
	runningTimeStr := os.Getenv("RUNNING_TIME_SECONDS")
	numClientsStr := os.Getenv("CLIENTS_NUMBER")
	piIterationStr := os.Getenv("PI_ITERATION")

	runningTime := 7
	if runningTimeStr != "" {
		if i, err := strconv.Atoi(runningTimeStr); err == nil {
			runningTime = i
		}
	}

	numClient := 40
	if numClientsStr != "" {
		if i, err := strconv.Atoi(numClientsStr); err == nil {
			numClient = i
		}
	}

	piIteration := 100000
	if piIterationStr != "" {
		if i, err := strconv.Atoi(piIterationStr); err == nil {
			piIteration = i
		}
	}

	wg := sync.WaitGroup{}
	stopCh := make(chan struct{})

	timer := time.NewTimer(time.Duration(runningTime) * time.Second)
	var requestsNumber uint64

	for i := 0; i < numClient; i++ {
		wg.Add(1)
		go func() {
			for {
				select {
				case <-stopCh:
					//fmt.Println("Stoping client...")
					wg.Done()
					return
				default:
					_, err := http.Get(fmt.Sprintf("%s/%d", serverBaseURL, piIteration))
					if err != nil {
						log.Print(err)
					}
					atomic.AddUint64(&requestsNumber, 1)
				}
			}
		}()
	}

	<-timer.C
	close(stopCh)

	wg.Wait()

	fmt.Printf("%d requests in %d seconds", requestsNumber, runningTime)
}
