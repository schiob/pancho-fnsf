from flask import Flask
app = Flask(__name__)

@app.route('/<int:iter_num>')
def calc_pi(iter_num):
    n = 2
    total = 3
    for x in range(iter_num):
        if x%2 == 0:
            total += (4 / (n*(n+1)*(n+2)))
        else:
            total -= (4 / (n*(n+1)*(n+2)))
        n += 2

    return str(total)

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0',processes=1,threaded=False)